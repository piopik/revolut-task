import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import SVGRemove from './svg/remove.svg';

import style from './Toast.style.scss';

const Toaster = ({
    title,
    text,
    color,
    action,
    actionName,
    onClose,
    position
}) => (
    <div className={cx(style.toast, style[`color-${color}`])} style={{transform: `translateY(-${position}px)`}}>
        <div className={style.content}>
            <div>
                <span className={style.title}>
                    {title}
                </span>
                <span className={style.text}>
                    {text}
                </span>
                <SVGRemove className={style.remove} onClick={onClose} onKeyPress={undefined} />
            </div>
        </div>
    </div>
);

Toaster.propTypes = {
    text: PropTypes.string.isRequired,
    color: PropTypes.oneOf(['green', 'red']),
    action: PropTypes.func,
    actionName: PropTypes.string,
    onClose: PropTypes.func,
    position: PropTypes.number
};

Toaster.defaultProps = {
    color: 'green',
    action: undefined,
    actionName: undefined,
    onClose: undefined,
    position: 0
};

export default Toaster;