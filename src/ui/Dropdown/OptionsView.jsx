import React from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import {Portal} from ".."

import style from './OptionsView.scss';

const OptionsView = ({
    options,
    active,
    getOptionsContainerRef,
    coordinates,
}) => (
    <Portal>
        <TransitionGroup>
            <CSSTransition
                key={active}
                classNames="anim"
                timeout={500}
                mountOnEnter
                unmountOnExit
            >
                {active ? (
                    <div className={style.root} style={coordinates} ref={getOptionsContainerRef} >
                        {
                            options.map(o => (
                                <div key={o.label} className={style.option} onClick={o.onClick}>
                                    {o.label}
                                </div>
                            ))
                        }
                    </div>
                ) : (
                    <div />
                )}
            </CSSTransition>
        </TransitionGroup>
    </Portal>
);

OptionsView.propTypes = {
    options: PropTypes.array.isRequired,
    active: PropTypes.bool,
    getOptionsContainerRef: PropTypes.func.isRequired
};

OptionsView.defaultProps = {
    active: false,
};

export default OptionsView;