import React, {PureComponent} from 'react'
import PropTypes from 'prop-types';

import OptionsView from './OptionsView';

import styles from './Dropdown.scss';

class DropdownContainer extends PureComponent {
    static handleClick(e) {
        e.nativeEvent.stopImmediatePropagation();
    }

    constructor(props) {
        super(props);

        this.state = {
            focused: false,
            touched: false,
            coordinates: {
                top: 0,
                left: 0,
                width: 0,
                height: 0,
            }
        };

        this.optionsRef = undefined;
        this.ref = undefined;
    }

    getOptionsContainerRef = (el) => {
        this.optionsRef = el;
    }

    clickHandler = (event) => {
        this.closeOptions();
        // if (event.type === 'click' && this.optionsRef && !this.optionsRef.contains(event.target)) {
        //     this.closeOptions();
        // }
    }

    openOptions = () => {
        if (this.state.focused) return false;

        document.addEventListener('click', this.clickHandler);

        this.setState({
            touched: true,
            focused: !this.state.focused
        });

        setTimeout(() => {
            if (this.state.focused && this.ref) {
                const
                    { left, width, top,  bottom } = this.ref.getBoundingClientRect(),
                    maxHeight = document.body.clientHeight - (bottom + window.scrollY) < 300 ? document.body.clientHeight - (bottom + window.scrollY) - 10 : 300;

                this.setState({
                    coordinates: {
                        top,
                        left,
                        width,
                        maxHeight,
                    }
                });
            }
        }, 100);
    }

    closeOptions = () => {
        if (!this.state.focused) return false;

        document.removeEventListener('click', this.clickHandler);

        this.setState({
            focused: false
        });
    }
    render() {
        const
            props = {
                options: this.props.options,
                active: this.state.focused,
                getOptionsContainerRef: this.getOptionsContainerRef,
                coordinates: this.state.coordinates,
            }

        return (
            <>
                <div className={styles.root} ref={(r) => this.ref=r} onClick={this.openOptions}>
                    {this.props.children}
                </div>
                <OptionsView {...props} />
            </>
        );
    }
}

DropdownContainer.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.any.isRequired,
        onClick: PropTypes.any.isRequired
    }))
};

DropdownContainer.defaultProps = {
    options: []
};

export default DropdownContainer;
