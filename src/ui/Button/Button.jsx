import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { Link } from 'react-router-dom';

import style from './Button.scss';

const Button = ({
    children,
    size,
    shadow,
    color,
    disabled,
    href,
    onClick
}) => {
    if (href) {
        return (
            <Link className={cx(style.button, style[`size-${size}`], style[`color-${color}`], (disabled && style.disabled), (shadow && style.shadow))} to={href}>
                { children }
            </Link>
        );
    }
    return (
        <button className={cx(style.button, style[`size-${size}`], style[`color-${color}`], (disabled && style.disabled), (shadow && style.shadow))} onClick={onClick} onKeyPress={undefined} >
            { children }
        </button>
    );
};

Button.propTypes = {
    children: PropTypes.any,
    color: PropTypes.oneOf(['white', 'rose']),
    size: PropTypes.oneOf(['medium', 'large']),
    shadow: PropTypes.bool,
    disabled: PropTypes.bool,
    href: PropTypes.string,
    onClick: PropTypes.func,
};

Button.defaultProps = {
    color: 'white',
    size: 'medium',
    shadow: true,
    disabled: false,
    href: undefined,
    onClick: undefined,
};

export default Button;