import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { COLORS } from 'constants/index';

import Chart from './svg/chart.svg';
import Ok from './svg/ok.svg';
import Plus from './svg/plus.svg';
import Search from './svg/search.svg';
import Switch from './svg/switch.svg';
import Wallet from './svg/wallet.svg';


import style from './Icon.style.scss';

/** Component printing SVG icon inline. */
const
    icons = {
        chart: <Chart />,
        ok: <Ok />,
        plus: <Plus />,
        search: <Search />,
        switch: <Switch />,
        wallet: <Wallet />,
    },
    Icon = ({icon, color, size}) => (
        <div className={cx(style.icon, style[`size-${size}`])} style={{'--color': COLORS[color] || color}}>
            {icons[icon]}
        </div>
    );

Icon.propTypes = {
    icon: PropTypes.string.isRequired,
    color: PropTypes.string,
    size: PropTypes.oneOf(['small', 'medium', 'semi-large', 'large'])
};

Icon.defaultProps = {
    color: 'black',
    size: 'medium'
};

export default Icon;