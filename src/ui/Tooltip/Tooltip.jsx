import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import { Portal } from '..';

import style from './Tooltip.scss';

class Tooltip extends PureComponent {
    constructor() {
        super();

        this.state = {
            open: false,
            coordinates: {},
        };

        this.handleMouseEnter = this.handleMouseEnter.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
    }

    getTooltipRef(el) {
        this.instance = el;
    }

    handleMouseEnter() {
        const elInfo = this.instance.getBoundingClientRect();

        this.setState({
            coordinates: {
                bottom: `${document.body.clientHeight - ((elInfo.top + window.pageYOffset) - 5)}px`,
                left: `${(elInfo.left - 100) + (elInfo.width / 2)}px`
            },
            open: !!this.props.text
        });
    }

    handleMouseLeave() {
        this.setState({
            open: false
        });
    }

    render() {
        const
            {text, children, position} = this.props,
            {open, coordinates} = this.state;

        return (
            <div className={cx(style.wrapper)} onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave} ref={el => this.getTooltipRef(el)}>
                {
                    text && (
                        <Portal>
                            <TransitionGroup>
                                <CSSTransition
                                    key={open}
                                    classNames="anim"
                                    timeout={500}
                                    mountOnEnter
                                    unmountOnExit
                                >
                                    {open ? (
                                        <div
                                            className={cx(style.tooltip, style[`position-${position}`])}
                                            style={{
                                                bottom: coordinates.bottom,
                                                left: coordinates.left
                                            }}
                                        >
                                            <div className={style.content}>
                                                {text}
                                            </div>
                                        </div>
                                    ) : (
                                        <div />
                                    )}
                                </CSSTransition>
                            </TransitionGroup>
                        </Portal>
                    )}
                {children}
            </div>
        );
    }
}

Tooltip.propTypes = {
    /** Tooltip text */
    text: PropTypes.string,
    /** Tooltip relative position */
    position: PropTypes.oneOf(['top', 'bottom', 'right']),
    /** Children nodes */
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired
};

Tooltip.defaultProps = {
    text: '',
    position: 'top'
};

export default Tooltip;