import React from 'react';
import PropTypes from 'prop-types';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import TutorialStep from './TutorialStep';
import TutorialBackground from './TutorialBackground';

import style from './Tutorial.style.scss';

const Tutorial = ({
    step,
    steps,
    element,
    next,
    close,
    finish
}) => {
    const
        props = {
            ...steps[step],
            element,
            next,
            close,
            finish
        },
        bgProps = {
            screenWidth: window.innerWidth,
            screenHeight: window.innerHeight,
            element
        };

    return (
        <div className={style.tutorial}>
            <TransitionGroup>
                <CSSTransition
                    key={step}
                    classNames="anim"
                    timeout={500}
                    mountOnEnter
                    unmountOnExit
                >
                    <TutorialStep {...props} />
                </CSSTransition>
            </TransitionGroup>
            <TutorialBackground {...bgProps} />
        </div>
    );
};

Tutorial.propTypes = {
    step: PropTypes.number,
    steps: PropTypes.array,
    element: PropTypes.object.isRequired,
    next: PropTypes.func,
    close: PropTypes.func,
    finish: PropTypes.func
};

Tutorial.defaultProps = {
    step: 0,
    steps: [],
    next: undefined,
    close: undefined,
    finish: undefined
};

export default Tutorial;