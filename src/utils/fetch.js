import axios from 'axios';

const
    defaultOptions = {
        baseURL: process.env.REACT_APP_CURRENCIES_API_URL,
        headers: {
            'Content-Type': 'application/json',
        },
    },
    Fetch = axios.create(defaultOptions);

export default Fetch;