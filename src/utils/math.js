export const roundToDecimals = (number, decimals) => {
    const base = Math.pow(10, decimals);
    return (Math.floor(number * base) / base);
}