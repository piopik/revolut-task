import React, {PureComponent} from 'react';
import { connect } from 'react-redux';

import { roundToDecimals } from 'utils/math';

import { actionsCurrencies } from 'store/actions/currencies'
import { actionsAccount } from 'store/actions/account'
import { getCurrencies } from 'store/selectors/currencies'

import Exchange from './Exchange';

const parseToValue = string => {
    if (!string.includes('.')) {
        return roundToDecimals(Number(string.replace(/[^\d.-]/g, ''))/100,2);
    }
    return roundToDecimals(Number(string.replace(/[^\d.-]/g, '')),2);
}

class ExchangeContainer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            currencyA: 'GBP',
            currencyB: 'USD',
            valueA: 0,
            valueB: 0,
            mode: 'exchanged'
        }
    }

    currencyAOptions = [
        {label: 'EUR', onClick: () => this.changeCurrencyA('EUR')},
        {label: 'GBP', onClick: () => this.changeCurrencyA('GBP')},
        {label: 'USD', onClick: () => this.changeCurrencyA('USD')}
    ]

    currencyBOptions = [
        {label: 'EUR', onClick: () => this.changeCurrencyB('EUR')},
        {label: 'GBP', onClick: () => this.changeCurrencyB('GBP')},
        {label: 'USD', onClick: () => this.changeCurrencyB('USD')}
    ]

    componentDidMount() {
        this.fetchRatio();
        this.interval = setInterval(this.fetchRatio, 10000);
    }

    componentDidUpdate(prevProps, prevState) {
        if (
            prevState.currencyA !== this.state.currencyA ||
            prevState.currencyB !== this.state.currencyB
        ) {
            this.fetchRatio();
            clearInterval(this.interval);
            this.interval = setInterval(this.fetchRatio, 10000);
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    changeValueA = ({target: {value}}) => {
        this.setState(() => ({
            valueA: parseToValue(value),
            mode: 'a'
        }))
    }

    changeValueB = ({target: {value}}) => {
        this.setState(() => ({
            valueB: parseToValue(value),
            mode: 'b'
        }))
    }

    changeCurrencyA = (value) => {
        this.setState(() => ({
            currencyA: value,
        }))
    }

    changeCurrencyB = (value) => {
        this.setState(() => ({
            currencyB: value,
        }))
    }

    switchCurrencies = () => {
        const { currencyA, currencyB } = this.state;
        this.setState(() => ({
            currencyA: currencyB,
            currencyB: currencyA,
        }))
    }

    exchange = () => {
        const { currencyA, currencyB, valueA, valueB, mode } = this.state;
        this.props.makeExchange({
            valueFrom: mode === 'a' ? valueA : undefined,
            valueTo: mode === 'b' ? valueB : undefined,
            currencyFrom: currencyA,
            currencyTo: currencyB
        });
    }

    fetchRatio = () => {
        const { currencyA } = this.state;

        this.props.fetchCurrencies(currencyA);
    }

    render() {
        const { currencyA, currencyB, mode } = this.state;
        const ratio = this.props.ratios[currencyA].rates[currencyB];

        const props = {
            changeValueA: this.changeValueA,
            changeValueB: this.changeValueB,
            switchCurrencies: this.switchCurrencies,
            exchange: this.exchange,
            currencyA: this.state.currencyA,
            currencyAOptions: this.currencyAOptions,
            currencyB: this.state.currencyB,
            currencyBOptions: this.currencyBOptions,
            valueA: (mode === 'a' || !ratio) ? this.state.valueA : this.state.valueB / ratio,
            valueB: (mode === 'b' || !ratio) ? this.state.valueB : this.state.valueA * ratio,
            ratio,
            isRatioLoading: this.props.ratios[currencyA].isLoading,
        };
        return <Exchange {...props} />;
    }
}

const
    mapStateToProps = state => ({
        ratios: getCurrencies(state)
    }),
    mapDispatchToProps = dispatch => ({
        fetchCurrencies: (currency) => dispatch(actionsCurrencies.fetchCurrency(currency)),
        makeExchange: (o) => dispatch(actionsAccount.makeExchange(o)),
    });


export default connect(mapStateToProps, mapDispatchToProps)(ExchangeContainer);
