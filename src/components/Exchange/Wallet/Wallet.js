import React from 'react';
import cx from 'classnames';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import style from './Wallet.scss';

const Wallet = ({
    GBP,
    USD,
    EUR,
}) => {
    return (
        <div className={style.root}>
            <div className={cx(style.currency, style[`trend-${GBP.trend}`])}>
                <div className={style.name}>
                    GBP
                </div>
                <div className={style.valueWrapper}>
                    <TransitionGroup component={null}>
                        <CSSTransition
                            key={GBP.value}
                            classNames="anim"
                            timeout={500}
                            mountOnEnter
                            unmountOnExit
                        >
                            <div className={style.value}>
                                {GBP.value.toFixed(2)}
                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                </div>
            </div>
            <div className={cx(style.currency, style[`trend-${USD.trend}`])}>
                <div className={style.name}>
                    USD
                </div>
                <div className={style.valueWrapper}>
                    <TransitionGroup component={null}>
                        <CSSTransition
                            key={USD.value}
                            classNames="anim"
                            timeout={500}
                            mountOnEnter
                            unmountOnExit
                        >
                            <div className={style.value}>
                                {USD.value.toFixed(2)}
                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                </div>
            </div>
            <div className={cx(style.currency, style[`trend-${EUR.trend}`])}>
                <div className={style.name}>
                    EUR
                </div>
                <div className={style.valueWrapper}>
                    <TransitionGroup component={null}>
                        <CSSTransition
                            key={EUR.value}
                            classNames="anim"
                            timeout={500}
                            mountOnEnter
                            unmountOnExit
                        >
                            <div className={style.value}>
                                {EUR.value.toFixed(2)}
                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                </div>
            </div>
        </div>
    )
}

export default Wallet;
