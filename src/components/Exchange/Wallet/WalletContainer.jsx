import React, {PureComponent} from 'react';
import { connect } from 'react-redux';

import { CURRENCIES } from 'constants/index';

import { getAccount } from 'store/selectors/account';

import Wallet from './Wallet';

class WalletContainer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            GBP: {
                value: 0,
                trend: 'no'
            },
            EUR: {
                value: 0,
                trend: 'no'
            },
            USD: {
                value: 0,
                trend: 'no'
            }
        }
    }

    static getDerivedStateFromProps(props, state) {
        const newState = {};

        CURRENCIES.forEach(({shortName}) => {
            if (props.account[shortName] !== state[shortName].value) {
                newState[shortName] = {
                    value: props.account[shortName],
                    trend:
                        (props.account[shortName] > state[shortName].value && 'up') ||
                        (props.account[shortName] < state[shortName].value && 'down') ||
                        'no'
                }
            }
        })

        return newState;
    }

    render() {
        const props = {
            GBP: this.state.GBP,
            USD: this.state.USD,
            EUR: this.state.EUR,
        };
        return <Wallet {...props} />;
    }
}

const mapStateToProps = state => ({
    account: getAccount(state)
});


export default connect(mapStateToProps, undefined)(WalletContainer);
