import React from 'react';
import { shallow } from 'enzyme';
import { createMockStore } from 'redux-test-utils';

import Exchange from './ExchangeContainer'

const
    store = {
        currencies: {
            EUR: {
                rates: {},
                isLoading: false,
            },
            GBP: {
                rates: {},
                isLoading: false,
            },
            USD: {
                rates: {},
                isLoading: false,
            }
        }
    };

describe('Exchange', () => {
    it('renders without crashing', () => {
        const comp = shallow(<Exchange store={createMockStore(store)} />).dive();
        expect(comp.length).toEqual(1);
    });

    it('switches choosen currencies', () => {
        const comp = shallow(<Exchange store={createMockStore(store)} />).dive();
        comp.instance().switchCurrencies();
        expect(comp.state('currencyA')).toEqual('USD');
        expect(comp.state('currencyB')).toEqual('GBP');
    });

    it('trigger fetching rates after currency change', () => {
        const fetchRatio = jest.fn();
        const comp = shallow(<Exchange store={createMockStore(store)} />).first().shallow();

        comp.instance().fetchRatio = fetchRatio;

        comp.instance().changeCurrencyA('EUR')

        expect(fetchRatio).toHaveBeenCalled();
    });
})

