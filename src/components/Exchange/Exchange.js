import React from 'react';
import cx from 'classnames';

import { Button, Dropdown, Icon } from 'ui';
import Wallet from './Wallet';

import style from './Exchange.scss';

const Exchange = ({
    valueA,
    valueB,
    currencyA,
    currencyAOptions,
    currencyB,
    currencyBOptions,
    changeValueA,
    changeValueB,
    ratio,
    isRatioLoading,
    switchCurrencies,
    exchange
}) => {
    return (
        <div className={style.root}>
            <div className={style.header}>
                Exchange
            </div>
            <div className={style.form}>
                <div className={style.card}>
                    <div className={style.field}>
                        <div className={style.left}>
                            <div className={style.label}>
                                From
                            </div>
                            <div className={style.name}>
                                { currencyA }
                                <div className={style.button}>
                                    <Dropdown options={currencyAOptions}>
                                        <Button>
                                            <Icon icon="search" color="blue" size="small"/>
                                        </Button>
                                    </Dropdown>
                                </div>
                            </div>
                        </div>
                        <div className={style.right}>
                            <input className={style.value} onChange={changeValueA} value={valueA.toFixed(2)}/>
                        </div>
                    </div>
                    <div className={style.separator}>
                        <div className={style.left}>
                            <div className={style.pill}>
                                <div className={cx(style.spinner, isRatioLoading && style.active)} />
                                <span>
                                    {ratio && ratio.toFixed(6)}
                                </span>
                                {/*<Icon icon="wallet" color="blue" size="small"/>*/}
                            </div>
                            <Button onClick={switchCurrencies}>
                                <Icon icon="switch" color="blue" size="small"/>
                            </Button>
                        </div>
                        <div className={style.right}>
                            <Button size="large" color="rose" shadow  onClick={exchange}>
                                <Icon icon="ok" color="white" size="large"/>
                            </Button>
                        </div>
                    </div>
                    <div className={style.field}>
                        <div className={style.left}>
                            <div className={style.name}>
                                { currencyB }
                                <div className={style.button}>
                                    <Dropdown options={currencyBOptions}>
                                        <Button>
                                            <Icon icon="search" color="blue" size="small"/>
                                        </Button>
                                    </Dropdown>
                                </div>
                            </div>
                            <div className={style.label}>
                                To
                            </div>
                        </div>
                        <div className={style.right}>
                            <input className={style.value} onChange={changeValueB} value={valueB.toFixed(2)}/>
                        </div>
                    </div>
                </div>
            </div>
            <Wallet />
        </div>
    )
}

export default Exchange;
