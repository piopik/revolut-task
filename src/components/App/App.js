import React from 'react';

import { Toaster } from 'ui';

import Exchange from '../Exchange'

import style from './App.scss';

const App = ({ toasts, isReady }) => {
    if (!isReady) {
        return (<div />)
    }
    return (
        <div className={style.app}>
            <div className={style.view}>
                <Exchange />
            </div>
            <Toaster toasts={toasts} />
            <div id="portal" />
        </div>
    )
}

export default App;
