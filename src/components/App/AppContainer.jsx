import React, {PureComponent} from 'react';
import { connect } from 'react-redux';

import { getSpinner, getToasts } from 'store/selectors/ui';

import App from './App';

class AppContainer extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isReady: false
        }
    }

    componentDidMount() {
        this.setState({
            isReady: true
        })
    }

    render() {
        const props = {
            toasts: this.props.toasts,
            isReady: this.state.isReady
        }
        return <App {...props} />;
    }
}

const
    mapStateToProps = state => ({
        toasts: getToasts(state),
        isSpinning: getSpinner(state)
    });


export default connect(mapStateToProps)(AppContainer);
