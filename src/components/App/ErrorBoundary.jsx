/* eslint-disable no-param-reassign */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    componentDidCatch(error, info) {
        super.componentDidCatch(error, info);
        this.setState({
            hasError: true
        });
    }

    render() {
        if (this.state.hasError) {
            return <div />;
        }
        return this.props.children;
    }
}

ErrorBoundary.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.arrayOf(PropTypes.node),
        PropTypes.node
    ]).isRequired,
};

ErrorBoundary.defaultProps = {};

export default ErrorBoundary;