const
    COLORS = {
        black: '#333333',
        grey: '#999999',
        greyLight: '#e5e5e5',
        ecru: '#f7f7f7',
        white: '#fff',
        rose: '#EB008D',
        blue: '#0075EB',
        yellow: '#F6CE42',
        red: '#D20000'
    },
    CURRENCIES = [
        {
            id: 'eur',
            shortName: 'EUR',
            name: 'Euro'
        },
        {
            id: 'gbp',
            shortName: 'GBP',
            name: 'Pound sterling'
        },
        {
            id: 'usd',
            shortName: 'USD',
            name: 'United States dollar'
        }
    ];

export {
    COLORS,
    CURRENCIES
}