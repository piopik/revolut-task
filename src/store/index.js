import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

import reducers from './reducers';

export default createStore(
    reducers,
    (process.env.REACT_APP_REDUX_DEVTOOLS && window.__REDUX_DEVTOOLS_EXTENSION__ ? // eslint-disable-line no-underscore-dangle
        compose(
            applyMiddleware(thunk),
            window.__REDUX_DEVTOOLS_EXTENSION__() // eslint-disable-line no-underscore-dangle
        )
        :
        applyMiddleware(thunk)
    )
);