import { combineReducers } from 'redux';

import account from './account.reducer';
import currencies from './currencies.reducer';
import ui from './ui.reducer';

export default combineReducers({
    account,
    currencies,
    ui,
});