import {
    SPINNER_PUSH,
    SPINNER_POP,
    TOASTER_PUSH,
    TOASTER_POP
} from '../actions/ui';

const
    initialState = {
        spinner: [],
        toaster: []
    },
    reducer = (state = initialState, action) => {
        switch (action.type) {
            case SPINNER_PUSH: {
                const spinner = [...state.spinner];
                spinner.push(true);
                return {...state, spinner};
            }
            case SPINNER_POP: {
                const spinner = [...state.spinner];
                spinner.pop();
                return {...state, spinner};
            }
            case TOASTER_PUSH: {
                const toaster = [...state.toaster];
                toaster.push({...action.toast, id: +new Date()});
                return {...state, toaster};
            }
            case TOASTER_POP: {
                const toaster = [...state.toaster];
                for (let i = 0; i < toaster.length; i += 1) {
                    if (toaster[i].id === action.id) {
                        toaster.splice(i, 1);
                    }
                }
                return {...state, toaster};
            }
            default:
                return state;
        }
    };

export default reducer;