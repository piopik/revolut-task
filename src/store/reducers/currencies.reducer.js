import { CURRENCY_LOADING, CURRENCY_UPDATE } from '../actions/currencies';

const
    initialState = {
        EUR: {
            rates: {},
            isLoading: false,
        },
        GBP: {
            rates: {},
            isLoading: false,
        },
        USD: {
            rates: {},
            isLoading: false,
        }
    },
    reducer = (state = initialState, action) => {
        switch (action.type) {
            case CURRENCY_LOADING:
                return {
                    ...state,
                    [action.base]: {
                        ...state[action.base],
                        isLoading: true,
                    }
                };
            case CURRENCY_UPDATE:
                return {
                    ...state,
                    [action.base]: {
                        rates: action.rates,
                        isLoading: false,
                    }
                };
            default:
                return state;
        }
    };

export default reducer;