import { ACCOUNT_INCREASE, ACCOUNT_DECREASE } from '../actions/account';

const
    initialState = {
        EUR: 200,
        GBP: 100,
        USD: 0
    },
    reducer = (state = initialState, action) => {
        switch (action.type) {
            case ACCOUNT_INCREASE:
                return {
                    ...state,
                    [action.currency]: state[action.currency] + action.value
                };
            case ACCOUNT_DECREASE:
                return {
                    ...state,
                    [action.currency]: state[action.currency] - action.value
                };
            default:
                return state;
        }
    };

export default reducer;