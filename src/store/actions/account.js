import { actionsUi } from './ui';

import { roundToDecimals } from 'utils/math';

export const ACCOUNT_INCREASE = 'ACCOUNT_INCREASE';
export const ACCOUNT_DECREASE = 'ACCOUNT_DECREASE';

export const actionsAccount = {
    makeExchange: ({valueFrom, valueTo, currencyFrom, currencyTo}) => (dispatch, getState) => {
        const { account, currencies } = getState(),
            ratio = currencies[currencyFrom].rates[currencyTo];
        let valueFromCalculated, valueToCalculated;

        if (valueFrom && !valueTo) {
            valueToCalculated = roundToDecimals(valueFrom * ratio, 2);
        } else if (!valueFrom && valueTo) {
            valueFromCalculated = roundToDecimals(valueTo / ratio, 2);
        } else {
            return
        }

        if (account[currencyFrom] < (valueFrom || valueFromCalculated)) {
            dispatch(actionsUi.toasterPush({
                title: 'Not enough resources on your account !',
                text: `You have only ${(account[currencyFrom])} ${currencyFrom}`
            }));
            return
        }

        dispatch({
            type: ACCOUNT_DECREASE,
            currency: currencyFrom,
            value: valueFrom || valueFromCalculated,
        });
        dispatch({
            type: ACCOUNT_INCREASE,
            currency: currencyTo,
            value: valueTo || valueToCalculated,
        });

        dispatch(actionsUi.toasterPush({
            title: 'Success!',
            text: `You have exchanged ${(valueFrom || valueFromCalculated)} ${currencyFrom} to ${valueTo || valueToCalculated} ${currencyTo}`
        }));
    }
};
