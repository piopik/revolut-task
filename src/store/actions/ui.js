export const TOASTER_PUSH = 'TOASTER_PUSH';
export const TOASTER_POP = 'TOASTER_POP';
export const SPINNER_PUSH = 'SPINNER_PUSH';
export const SPINNER_POP = 'SPINNER_POP';

export const actionsUi = {
    spinnerPush: () => ({
        type: SPINNER_PUSH
    }),
    spinnerPop: () => dispatch => {
        setTimeout(() => dispatch({type: SPINNER_POP}), 200);
    },
    toasterPush: toast => ({
        type: TOASTER_PUSH,
        toast
    }),
    toasterPop: id => ({
        type: TOASTER_POP,
        id
    })
};