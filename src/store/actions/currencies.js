import currenciesService from 'services/currenciesService'

import { actionsUi } from './ui';

export const CURRENCY_LOADING = 'CURRENCY_LOADING';
export const CURRENCY_UPDATE = 'CURRENCY_UPDATE';

export const actionsCurrencies = {
    fetchCurrency: (currency) => dispatch => {
        dispatch(actionsUi.spinnerPush());
        dispatch({
            type: CURRENCY_LOADING,
            base: currency
        })
        return new Promise((resolve, reject) => {
            currenciesService.get(currency)
                .then(({data: {base, rates}}) => {
                    dispatch(actionsUi.spinnerPop());
                    dispatch({
                        type: CURRENCY_UPDATE,
                        base,
                        rates
                    })
                })
                .catch(() => {
                    dispatch(actionsUi.spinnerPop());
                    dispatch(actionsUi.toasterPush({
                        text: 'Rates fetch failed'
                    }));
                    reject();
                })
        })
    }
};
