import { createSelector } from 'reselect';

const currenciesFunc = state => state.currencies;

export const getCurrencies = createSelector(
    [currenciesFunc],
    (currencies) => currencies
);