import { createSelector } from 'reselect';

const uiFunc = state => state.ui,
    getToasts = createSelector(
        [uiFunc],
        ({toaster}) => toaster
    ),

    getSpinner = createSelector(
        [uiFunc],
        ({spinner}) => !!spinner.length
    );

export {
    getToasts,
    getSpinner
};