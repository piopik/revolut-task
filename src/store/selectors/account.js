import { createSelector } from 'reselect';

const accountFunc = state => state.account;

export const getAccount = createSelector(
    [accountFunc],
    (account) => account
);