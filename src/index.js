import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom';

import store from './store';
import * as serviceWorker from './serviceWorker';

import App from './components/App';
import ErrorBoundary from './components/App/ErrorBoundary';

ReactDOM.render(
    (
        <Provider store={store}>
            <BrowserRouter>
                <ErrorBoundary>
                    <App />
                </ErrorBoundary>
            </BrowserRouter>
        </Provider>
    ),
    document.getElementById('root')
);


serviceWorker.unregister();
