import fetch from 'utils/fetch';

export default {
    get: base => {
        // hacky workaround -> the API seems to have problem if base param is equal to 'EUR' and symbols include 'EUR' as well
        if (base === 'EUR') {
            return new Promise((resolve, reject) => {
                fetch.get('latest', {params: {base, symbols: 'GBP,USD'}})
                    .then(({data}) => {
                        const d = {...data}
                        d.rates.EUR = 1;
                        resolve({data: d})
                    })
                    .catch(() => {
                        reject();
                    })
            })
        }

        return fetch.get('latest', {params: {base, symbols: 'GBP,EUR,USD'}})
    }
};